<?php

/**
 * @file
 * Rules integration for the rules_flood module.
 */

/* Action, condition, and data type definitions. */

/**
 * Implements hook_rules_action_info().
 */
function rules_flood_rules_action_info() {
  $actions = array();

  // Clear a flood event.
  $actions['rules_flood_flood_clear_event'] = array(
    'label' => t('Clear event'),
    'group' => t('Rules flood'),
    'parameter' => array(
      'name' => array(
        'type' => 'text',
        'label' => t('Name'),
        'description' => t('The unique name of the event.'),
      ),
      'identifier' => array(
        'type' => 'ip_address',
        'label' => t('Identifier'),
        'description' => t('Unique identifier of the current user. Defaults to their IP address.'),
        'optional' => TRUE,
        'default value' => NULL,
      ),
    ),
  );

  // Register a flood event.
  $actions['rules_flood_flood_register_event'] = array(
    'label' => t('Register event'),
    'group' => t('Rules flood'),
    'parameter' => array(
      'name' => array(
        'label' => t('Name'),
        'type' => 'text',
        'description' => t('The unique name of the event.'),
      ),
      'window' => array(
        'label' => t('Window'),
        'type' => 'integer',
        'description' => t('Number of seconds in the time window for this event (default is 3600 seconds, or 1 hour).'),
        'optional' => TRUE,
        'default value' => 3600,
      ),
      'identifier' => array(
        'label' => t('Identifier'),
        'type' => 'ip_address',
        'description' => t('Unique identifier of the current user. Defaults to their IP address.'),
        'optional' => TRUE,
        'default value' => NULL,
      ),
    ),
  );

  return $actions;
}

/**
 * Implements hook_rules_condition_info().
 */
function rules_flood_rules_condition_info() {
  $conditions = array();

  // Check if a flood event is allowed.
  $conditions['rules_flood_flood_is_allowed'] = array(
    'label' => t('Flood is allowed'),
    'group' => t('Rules flood'),
    'parameter' => array(
      'name' => array(
        'label' => t('Name'),
        'type' => 'text',
        'description' => t('The unique name of the event.'),
      ),
      'threshold' => array(
        'label' => t('Threshold'),
        'type' => 'integer',
        'description' => t('The maximum number of times each user can do this event per time window.'),
      ),
      'window' => array(
        'label' => t('Window'),
        'type' => 'integer',
        'description' => t('Number of seconds in the time window for this event (default is 3600 seconds, or 1 hour).'),
        'optional' => TRUE,
        'default value' => 3600,
      ),
      'identifier' => array(
        'label' => t('Identifier'),
        'type' => 'ip_address',
        'description' => t('Unique identifier of the current user. Defaults to their IP address.'),
        'optional' => TRUE,
        'default value' => NULL,
      ),
    ),
  );

  return $conditions;
}

/* Action and condition implementations. */

/**
 * Make the flood control mechanism forget an event for the current visitor.
 *
 * @param string $name
 *   The name of an event.
 * @param string|null $identifier
 *   Optional identifier (defaults to the current user's IP address).
 */
function rules_flood_flood_clear_event($name, $identifier = NULL) {
  // Ensure the parameters are the correct data type for the function we want
  // to call.
  $name = (string) $name;
  if (empty($identifier)) {
    $identifier = ip_address();
  }
  elseif (!is_string($identifier)) {
    $identifier = (string) $identifier;
  }

  // Perform the action.
  flood_clear_event($name, $identifier);
}

/**
 * Registers an event for the current visitor to the flood control mechanism.
 *
 * @param string $name
 *   The name of an event.
 * @param int $window
 *   Optional number of seconds before this event expires. Defaults to 3600 (1
 *   hour). Typically uses the same value as the flood_is_allowed() $window
 *   parameter. Expired events are purged on cron run to prevent the flood table
 *   from growing indefinitely.
 * @param string|null $identifier
 *   Optional identifier (defaults to the current user's IP address).
 */
function rules_flood_flood_register_event($name, $window = 3600, $identifier = NULL) {
  // Ensure the parameters are the correct data type for the function we want
  // to call.
  $name = (string) $name;
  if (!is_int($window)) {
    $window = (int) $window;
  }
  if (empty($identifier)) {
    $identifier = ip_address();
  }
  elseif (!is_string($identifier)) {
    $identifier = (string) $identifier;
  }

  // Perform the action.
  flood_register_event($name, $window, $identifier);
}

/**
 * Checks whether a user is allowed to proceed with the specified event.
 *
 * @param string $name
 *   The name of an event.
 * @param int $threshold
 *   The maximum number of times each user can do this event per time window.
 * @param int $window
 *   Optional number of seconds before this event expires. Defaults to 3600 (1
 *   hour). Typically uses the same value as the flood_is_allowed() $window
 *   parameter. Expired events are purged on cron run to prevent the flood table
 *   from growing indefinitely.
 * @param string|null $identifier
 *   Optional identifier (defaults to the current user's IP address).
 *
 * @return bool
 *   TRUE if the user is allowed to proceed. FALSE if they have exceeded the
 *   threshold and should not be allowed to proceed.
 */
function rules_flood_flood_is_allowed($name, $threshold, $window = 3600, $identifier = NULL) {
  // Ensure the parameters are the correct data type for the function we want
  // to call.
  $name = (string) $name;
  $threshold = (int) $threshold;
  if (!is_int($window)) {
    $window = (int) $window;
  }
  if (empty($identifier)) {
    $identifier = ip_address();
  }
  elseif (!is_string($identifier)) {
    $identifier = (string) $identifier;
  }

  // Evaluate the condition.
  return flood_is_allowed($name, $threshold, $window, $identifier);
}
